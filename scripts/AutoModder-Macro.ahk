; Start the script with the application running and focus in the "Bundle File Name" on the "Create Mod" tab.

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
#SingleInstance force
SetTitleMatchMode Regex

; Variables
CSVFile = %A_ScriptDir%\list.csv
MainWindow = PAYDAY Bundle Modder 1.14 ahk_class WindowsForms10.Window.8.app.0.2bf8098_r11_ad1
SleepShort = 20
SleepMedium = 85
SleepLong = 110

; Make sure the CSV File exists
IfNotExist, %CSVFile%
{
	MsgBox Input file not found: %CSVFile%
	ExitApp
}

; Record start time
StartTime := A_TickCount

; count lines in file (by SKAN) for progress bar
FileRead, f1, %CSVFile%
StringReplace,OutputVar,f1,`n,`n,useerrorlevel
Lines:=ErrorLevel + 1

; Display a progress bar
Gui, Add, Progress, w300 Range0-%Lines% vProgressBar
Gui, +AlwaysOnTop +Disabled -SysMenu +Owner
Gui, Show, NoActivate, Processing CSV File...


; Actions to perform on each line of CSVFile
Loop, Read, %CSVFile%
{
	; Parse line from file
	LineNumber = %A_Index%
	Loop, parse, A_LoopReadLine, CSV
	{
		if (A_Index = 1)
			origFile = %A_LoopField%
		else if (A_Index = 2)
			newFile = %A_LoopField%
	}

	; Activate application if it isn't the active window
	IfWinNotActive, %MainWindow%
		WinActivate, %MainWindow%

	If WinActive(MainWindow)
	{
		BlockInput On

		; Set Original Filename from bundle
		Send %origFile%
		Sleep SleepMedium

		; Set Replacement File
		Send {Tab}{Tab}{Tab}
		Sleep SleepShort
		Send {Space}
		Sleep SleepLong
		Send %newFile%{enter}
		Sleep SleepShort

		; Add Replacement to mod list
		Send {Tab}{Tab}
		Sleep SleepShort
		Send {Space}
		Sleep SleepShort

		; Move onto the next file
		Send ^{Tab}
		Sleep SleepShort
		Send ^+{Tab}
		Sleep SleepShort
		Send {Tab}^a{Backspace}
		Sleep SleepMedium
		
		GuiControl,, ProgressBar, +1
	}
	Else
	{
		Gui, Hide
		ElapsedTime := FormatSeconds((A_TickCount - StartTime)//1000)
		MsgBox,4112,Error, The intended application is not the active window, exiting.`n`nLine Number: %LineNumber%`nColumn A: %origFile%`nColumn B: %newFile%`nElapsed Time: %ElapsedTime%
		ExitApp
	}
}
Gui, Hide
ElapsedTime := FormatSeconds((A_TickCount - StartTime)//1000)
MsgBox Finished processing %CSVFile%.`n`nNumber of lines: %LineNumber%`nElapsed Time: %ElapsedTime%
ExitApp


; Hotkey to exit script
^!z::
	ElapsedTime := FormatSeconds((A_TickCount - StartTime)//1000)
	MsgBox,4160,CSV Processing,Exiting application at user's request.`n`nLine Number: %LineNumber%`nColumn A: %origFile%`nColumn B: %newFile%`nElapsed Time: %ElapsedTime%
ExitApp


; Convert the specified number of seconds to hh:mm:ss format.
FormatSeconds(NumberOfSeconds)
{
    time = 19990101  ; *Midnight* of an arbitrary date.
    time += %NumberOfSeconds%, seconds
    FormatTime, mmss, %time%, mm:ss
    return NumberOfSeconds//3600 ":" mmss  ; This method is used to support more than 24 hours worth of sections.
}