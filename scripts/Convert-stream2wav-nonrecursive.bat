@ECHO OFF
:: Send all converted to .\converted folder, leave the rest behind

MKDIR .\converted
REN *.stream *.wav
FOR %%F IN (*.wav) DO "%SyncHome%\Tools\Games\Payday\sound_converter\wwise_ima_adpcm.exe" -d "%%F" ".\converted\%%F"
DIR .\converted /B  > .\converted.txt
FOR /F "tokens=*" %%A IN (.\converted.txt) DO DEL "%%A"


FOR /R %1 %%F IN (*.stream) do wwise_ima_adpcm.exe -d "%%F" "%%~dpnF.wav"