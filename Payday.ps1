﻿# Suite of scripts for PAYDAY audio modding

Function Setup-ModEnvironment($PDModDir = $PDModDir, $ToolsDir = $ToolsDir){
    # Create folders to hold the mod
    New-Item -ItemType Directory -Path "$PDModDir\source" -Force
    
    # Copy the silence files
    Copy-Item "$ToolsDir\silence" -Destination "$PDModDir\silence" -Recurse -Filter '*.stream'
}


Function Prepare-Files($SourceDir = $SourceDir, $Converter = $Converter)
{
    # Recusively rename all .stream files to .wav
    Get-ChildItem $SourceDir -Recurse -Filter '*.stream' | Rename-Item -NewName { $_.Name.replace(".stream",".wav") }

    # Create "converted" folder inside each folder (only 1 level, not recursive)
    Get-ChildItem $SourceDir | Where-Object {($_.PSIsContainer)} | % {$_.FullName} | % {New-Item -ItemType Directory -Path "$_\converted" -ErrorAction Ignore}

    # Decode ALL files recursively. Successful decodes are strored in the "converted" folder
    Get-ChildItem $SourceDir -Recurse -Filter '*.wav' | ForEach-Object {&$Converter "-d" $($_.FullName) $($_.DirectoryName + "\converted\" + $_.Name)} #| Out-Null

    # Identify which files were successfully converted and remove them from the root directory
    Get-ChildItem $SourceDir -Recurse -Filter 'converted' | Where-Object {($_.PSIsContainer)} | Get-ChildItem -Filter '*.wav' | ForEach-Object {Remove-Item -Path "$(Split-Path $_.DirectoryName -Parent)\$($_.Name)"}
}


Function Generate-CSV($SourceDir = $SourceDir, $CSVFile = "d:\data.csv")
{
    Import-Module $env:apps\Scripts\Modules\Media\MPTag\mptag
    $Results = @()
    $Files = Get-ChildItem -Path "$SourceDir\*.wav" -Recurse

    foreach($File in $Files)
    {
      if ($File.DirectoryName -Match ".\\converted") { $Converted = "TRUE"; $Folder = Split-Path $File.Directory -Parent | Split-Path -leaf } else { $Converted = "FALSE"; $Folder = Split-Path $File.Directory -Leaf }
      $MediaInfo = $File | Get-MediaInfo | Select Duration, AudioBitrate, AudioSampleRate, AudioChannels

      $Properties = @{
        Name = $File.name
        Word = ""
        Character = ""
        Text = ""
        Action = ""
        ReplacedBy = ""
        Size = $File.length
        Duration = $MediaInfo.duration
        Bitrate = $MediaInfo.AudioBitrate
        SampleRate = $MediaInfo.AudioSampleRate
        Channels = $MediaInfo.AudioChannels
        Directory = $Folder
        Converted = $Converted
      }
      $Results += New-Object psobject -Property $Properties
    }
    $Results | Select-Object Name,Word,Character,Text,Action,ReplacedBy,Size,Duration,Bitrate,SampleRate,Channels,Directory,Converted | Export-Csv -notypeinformation -Path $CSVFile
}


# ReEncode-Sounds -Converter $Converter -Path "D:\pdmods\FilthyFilter"
Function ReEncode-Sounds($Converter = $Converter, $Path = $PDModDir)
{
    # Can add -Recurse to Get-ChildItem if necessary.
    # If you get an error similar to this: "[ERROR] Input file must be a well formed signed 16bit PCM WAV file" it is probably already a stream file, and will be renamed without problem.
    Get-ChildItem "$Path" -Filter '*.wav' | ForEach-Object {Write-Output "Converting $($_.FullName)"; &$Converter "-e" $($_.FullName) $($_.DirectoryName + "\" +  $_.BaseName + ".stream")}
}


$PDModDir = "D:\pdmods"
$SourceDir = "$PDModDir\source"
$ToolsDir = "$env:SyncHome\Tools\Games\Payday\Tools"
$Converter = "$ToolsDir\sound_converter\wwise_ima_adpcm.exe"