@ECHO ON
SET SourceDir=D:\Mods\New
ECHO Run on this Source Directoery? %SourceDir%
PAUSE

:: Make sure SourceDir's drive is the working drive.
%SourceDir:~0,2%


:: Recusively rename all .stream files to .wav
REM FOR /R %SourceDir% %%G in (*.stream) DO REN "%%G" "%%~nG.wav"

:: Create "converted" folder inside each folder (only 1 level, not recursive)
REM FOR /F "tokens=*" %%G in ('DIR /B /A:D "%SourceDir%\*"') DO MKDIR "%SourceDir%\%%G\converted"

:: Decode ALL files recursively. Successful decodes are strored in the "converted" folder
REM FOR /R %SourceDir% %%F IN (*.wav) DO "%SyncHome%\Tools\Games\Payday\sound_converter\wwise_ima_adpcm.exe" -d "%%F" "%%~dpFconverted\%%~nxF"

:: Find the successfully converted files
FOR /F "tokens=*" %%G in ('DIR /B /A:D "%SourceDir%\*"') DO DIR /B "%SourceDir%\%%G\converted" >> "%SourceDir%\converted.txt" 2>&1
PAUSE
:: Remove the successfully decoded files from the root folders
FOR /F "tokens=*" %%A IN (%SourceDir%\converted.txt) DO DEL "%%A"