; Start the script with the application running and focus in the "Bundle File Name" on the "Create Mod" tab.

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
#SingleInstance force
SetTitleMatchMode Regex

; Variables
CSVFile = %A_ScriptDir%\list.csv
MainWindow = ^PAYDAY 2 Bundle Modder* ahk_class WindowsForms10.Window.8.app.0.2bf8098_r11_ad1
MainWindowTab = ahk_class WindowsForms10.Window.8.app.0.2bf8098_r11_ad1
MainBundleFileText = WindowsForms10.EDIT.app.0.2bf8098_r11_ad14
MainBrowseButton = WindowsForms10.BUTTON.app.0.2bf8098_r11_ad14
MainAddModButton = WindowsForms10.BUTTON.app.0.2bf8098_r11_ad16
BrowseWindow = Open ahk_class #32770
BrowseFileText = Edit1
BrowseOkayButton = Button1

; Make sure the CSV File exists
IfNotExist, %CSVFile%
{
	MsgBox Input file not found: %CSVFile%
	ExitApp
}

; Make sure main window is present
WinWait, %MainWindow%,, 1
if ErrorLevel
{
	MsgBox, "Couldn't find main application window."
	ExitApp
}

; Record start time
StartTime := A_TickCount

; count lines in file (by SKAN) for progress bar
FileRead, f1, %CSVFile%
StringReplace,OutputVar,f1,`n,`n,useerrorlevel
Lines:=ErrorLevel + 1

; Display a progress bar
SetFormat, Float, 1.1 ; clean up percentage complete
Gui, Add, Progress, w300 Smooth Range0-%Lines% vProgressBar
Gui, Add, Text, xp yp+2 hp wp Center BackgroundTrans vPercent, 0`%
Gui, Add, Text, yp+18 wp Center BackgroundTrans vCurrent
Gui, +AlwaysOnTop +Disabled -SysMenu +Owner
Gui, Show, NoActivate, Processing CSV File...


; Actions to perform on each line of CSVFile
Loop, Read, %CSVFile%
{
	; Parse line from file
	LineNumber = %A_Index%
	Loop, parse, A_LoopReadLine, CSV
	{
		if (A_Index = 1)
			origFile = %A_LoopField%
		else if (A_Index = 2)
			newFile = %A_LoopField%
	}


	; Activate application if it isn't the active window
	IfWinNotActive, %MainWindow%
		WinActivate, %MainWindow%

	If WinActive(MainWindow)
	{
		; Set original filename from bundle
		WinWaitActive, %MainWindow%
		{
			ControlSetText, %MainBundleFileText%, %origFile%, %MainWindow%
			ControlSend, %MainBrowseButton%, {Space}, %MainWindow%
		}
		; Set the new replacement file
		WinWaitActive, %BrowseWindow%
		{
			ControlSetText, %BrowseFileText%, %newFile%, %BrowseWindow%
			ControlSend, %BrowseOkayButton%, {Space}, %BrowseWindow%
		}
		; Add Replacement to mod list
		WinWaitActive, %MainWindow%
			ControlSend, %MainAddModButton%, {Space}, %MainWindow%

		; Update progress bar
		GuiControl,, ProgressBar, +1
		GuiControl,, Percent, % 100*(LineNumber/Lines)`%
		GuiControl,, Current, %LineNumber%/%Lines%
	}
	Else
	{
		Gui, Hide
		ElapsedTime := FormatSeconds((A_TickCount - StartTime)//1000)
		MsgBox,4112,Error, The intended application is not the active window, exiting.`n`nLine Number: %LineNumber%`nColumn A: %origFile%`nColumn B: %newFile%`nElapsed Time: %ElapsedTime%
		ExitApp
	}
}
Gui, Hide
ElapsedTime := FormatSeconds((A_TickCount - StartTime)//1000)
MsgBox Finished processing %CSVFile%.`n`nNumber of lines: %LineNumber%`nElapsed Time: %ElapsedTime%
ExitApp


; Hotkey to exit script
^!z::
Gui, Hide
	ElapsedTime := FormatSeconds((A_TickCount - StartTime)//1000)
	MsgBox,4160,CSV Processing,Exiting application at user's request.`n`nLine Number: %LineNumber%`nColumn A: %origFile%`nColumn B: %newFile%`nElapsed Time: %ElapsedTime%
ExitApp


; Convert the specified number of seconds to hh:mm:ss format.
FormatSeconds(NumberOfSeconds)
{
		time = 19990101  ; *Midnight* of an arbitrary date.
		time += %NumberOfSeconds%, seconds
		FormatTime, mmss, %time%, mm:ss
		return NumberOfSeconds//3600 ":" mmss  ; This method is used to support more than 24 hours worth of sections.
}