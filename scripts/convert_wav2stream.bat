@echo off
:: Re-encode all wav files back to stream

for /R %1 %%F in (*.wav) do wwise_ima_adpcm.exe -e "%%F" "%%~dpnF.stream"