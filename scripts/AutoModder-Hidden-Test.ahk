; Start the script with the application running and focus in the "Bundle File Name" on the "Create Mod" tab.

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#SingleInstance force
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
DetectHiddenWindows, On
SetTitleMatchMode Regex

; Variables
CSVFile = %A_ScriptDir%\list.csv
MainWindow = PAYDAY Bundle Modder 1.14 ahk_class WindowsForms10.Window.8.app.0.2bf8098_r11_ad1
MainBundleFileText = WindowsForms10.EDIT.app.0.2bf8098_r11_ad14
MainBrowseButton = WindowsForms10.BUTTON.app.0.2bf8098_r11_ad15
MainAddModButton = WindowsForms10.BUTTON.app.0.2bf8098_r11_ad17
BrowseWindow = Open ahk_class #32770
BrowseFileText = Edit1
BrowseOkayButton = Button1

; Make sure the CSV File exists
IfNotExist, %CSVFile%
{
	MsgBox Input file not found: %CSVFile%
	ExitApp
}

; Make sure main window is present and disable user interaction with it
WinWait, %MainWindow%,, 1
if ErrorLevel
	MsgBox, "Couldn't find main application window."
WinSet, Disable,, %MainWindow%
WinHide %MainWindow%

; Record start time
StartTime := A_TickCount

; count lines in file (by SKAN) for progress bar
FileRead, f1, %CSVFile%
StringReplace,OutputVar,f1,`n,`n,useerrorlevel
Lines:=ErrorLevel + 1

; Display a progress bar
Gui, Add, Progress, w300 Range0-%Lines% vProgressBar
Gui, +AlwaysOnTop +Disabled -SysMenu +Owner
Gui, Show, NoActivate, Processing CSV File...


; Actions to perform on each line of CSVFile
Loop, Read, %CSVFile%
{
	; Parse line from file
	LineNumber = %A_Index%
	Loop, parse, A_LoopReadLine, CSV
	{
		if (A_Index = 1)
			origFile = %A_LoopField%
		else if (A_Index = 2)
			newFile = %A_LoopField%
	}


	; Set original filename from bundle
	WinWait, %MainWindow%,, 1
	if ErrorLevel
		MsgBox, "Couldn't find main application window."
	else
	{
		ControlSetText, %MainBundleFileText%, %origFile%, %MainWindow%
		ControlSend, %MainBrowseButton%, {Space}, %MainWindow%
	}


	; Set the new replacement file
	WinWait, %BrowseWindow%,, 1
	if ErrorLevel
		MsgBox, "Couldn't Find browse window."
	else {
		WinSet, Disable,,%BrowseWindow%
		;WinHide %BrowseWindow%
		ControlSetText, %BrowseFileText%, %newFile%, %BrowseWindow%
		ControlSend, %BrowseOkayButton%, {Space}, %BrowseWindow%
		
		; Add Replacement to mod list
		Sleep 10
		ControlSend, %MainAddModButton%, {Space}, %MainWindow%
	}


	; Update progress bar
	GuiControl,, ProgressBar, +1

	
	;Else
	;{
;		Gui, Hide
;		ElapsedTime := FormatSeconds((A_TickCount - StartTime)//1000)
;		MsgBox,4112,Error, The intended application is not the active window, exiting.`n`nLine Number: %LineNumber%`nColumn A: %origFile%`nColumn B: %newFile%`nElapsed Time: %ElapsedTime%
;		ExitApp
;	}
}
Gui, Hide
ElapsedTime := FormatSeconds((A_TickCount - StartTime)//1000)
MsgBox Finished processing %CSVFile%.`n`nNumber of lines: %LineNumber%`nElapsed Time: %ElapsedTime%
ExitApp


; Hotkey to exit script
^!z::
Gui, Hide
	ElapsedTime := FormatSeconds((A_TickCount - StartTime)//1000)
	MsgBox,4160,CSV Processing,Exiting application at user's request.`n`nLine Number: %LineNumber%`nColumn A: %origFile%`nColumn B: %newFile%`nElapsed Time: %ElapsedTime%
ExitApp


; Convert the specified number of seconds to hh:mm:ss format.
FormatSeconds(NumberOfSeconds)
{
    time = 19990101  ; *Midnight* of an arbitrary date.
    time += %NumberOfSeconds%, seconds
    FormatTime, mmss, %time%, mm:ss
    return NumberOfSeconds//3600 ":" mmss  ; This method is used to support more than 24 hours worth of sections.
}