# PAYDAY: The Heist - Filthy Filter
I created these mods because I didn't like hearing all the "strong language" in PAYDAY: The Heist. I find the game to be much more enjoyable, and hopefully you will too!

## [Tools](https://bitbucket.org/zabb65/payday-2-modding-information/downloads)
* **pdthbundletool** - Unpack bundles
* **sound_converter** - Convert unpacked .stream files to wav and back again
* **pdthmod_tool** - Create and apply mod packs

## Apply Mod
1. Rename/Copy: Required by the Modder (Should go away when PAYDAY the Heist is officially supported by the mod tool)
    1. copy .\assets\all.blb .\assets\bundle_db.blb
2. Run pdthmod_tool
    1. Set the assets folder (steamapps\common\PAYDAY The Heist\assets)
    2. Apply the mod

## Mod Creation Steps
0. Extract all bundles
    1. Extract the "pdthbundletool" to SteamApps\Common\PAYDAY The Heist\assets
    2. From the command prompt in the assets directory, run: pdthbundle.exe -update -extract_all
1. Move "extract" folder and copy the files of interest to (D:\pdmods\source)
    1. soundbanks\streamed\*.vox
2. Run the functions in the Scripts.ps1 file (Don't forget to dot source the file first.)
    1. Setup-ModEnvironment
        1. Create folders and copy silence files.
        2. Prepare-Files
            1. Renames all stream files to wav
            2. Convert all wav files and move the successful conversions to \converted\ folder
            3. Unconverted stream files are left in the root
        3. Generate-CSV
            1. Create a CSV file to be imported to a sheet based off of TEMPLATE in Sounds.xlsx
3. Document each file of interest by filling out the information on the sheet
    a. To take a break, move ones that you have finished into a "GoneThrough" folder. Just move them back when you are done
4. Run the "Organize Source Files" Commands from Sounds.xlsx to put files in their WORD folders
5. Run the "Copy Mod Files" Commands from Sounds.xlsx to copy all needed files to the mod working area
6. Run the "ReEncode-Sounds" function from the PowerShell Scripts.ps1
7. Compile mod using one of the below methods

### Compile Mod - JSON (Recommended)
1. Fill in steps here
2. and here

### Compile Mod - AutoModder (Deprecated)
1. Make a CSV from the "CSV" column in Sounds.xlsx
2. Run AutoModder.ahk with the CSV file from step 7
3. Fill out the mod details, and make sure to include a note about require all_X

## Commands
#####  Create csv files with the name and size (bytes), and name and length (duration in seconds)            
```
#!powershell

Import-Module $env:apps\Scripts\Modules\Media\MPTag\mptag
Get-ChildItem .\ | Select-Object name, length | export-csv 'd:\bytes.csv' -encoding ascii -NoTypeInformation    
Get-ChildItem .\* | Get-MediaInfo | select name, duration, AudioBitrate, AudioChannels, AudioSampleRate | export-csv 'd:\list.csv' -NoTypeInformation
```

##### Zip all unconverted streams from file
` 7za.exe a -t7z .\unconverted.7z @unconvertedStreams.txt `

##### Excel: Remove "converted\" from path
` =SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(CSVReplace,".1",""),".stream",".wav") `

##### Copy original files (useful to isolate a characters files with filters)
` ="Copy-Item C:\Users\rfrost\Downloads\regular_vox8152014\"&IF(Converted,"converted\","")&IF(Word<>"",Word&"\","")&File&" d:\testing" `


### Silence
The Silence folder contains silent audio clips.  
The following table shows files that are convertible/playable.  
*Note: All other combinations are replaced with the silence.stream file.*

Bit Rate  |  Sample Rate
----------|-------------
6         | 375
256       | 16000
384       | 24000
512       | 16000
512       | 32000
576       | 36000
706       | 44100
768       | 48000
1411      | 44100


### Conventions & Notes
* Backup edited files: ` name-copy.wav `
* Each map folder will have sub-directories where files with foul words will be stored and edited if applicable
* If a word does not have a replacement, it will be replaced by very brief silence of a file with the same properties
* All un-playable/un-convertable files are assumed to be _foul_ and will be replaced by silence (aggressive)
* Unconverted streams: **Only 706 44100 are playable** (_and not all of those are playable_)
* FilthyFilter-Clean won't work unless you remove the password from the archive, possibly due to the number of files modified
    * Zip Password (_bundle tools/PDBundleModder/PDBundleModPatcher/Form1.cs_): ` 0$45'5))66S2ixF51a<6}L2UK `

##### Problem conversions:
203582935.1.wav	Co	Hoxton	On the ground you [].	Replace	310867345.1.wav	160684	00:01.8	706	44100  
341924207.1.wav	Ba	Hoxton	Get down on the ground you [].	Edit	341924207.1.wav	158892	00:01.8	706	44100  
628653644.1.wav	Ba	Hoxton	Hands up you [].	Edit	628653644.1.wav	107692	00:01.2	706	44100