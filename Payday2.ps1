﻿#############################################
# Suite of scripts for PAYDAY audio modding
#
# Written By: Richard Frost
#############################################


Function Setup-ModEnvironment($PDModDir = $PDModDir, $SourceDir = $SourceDir){
    # Create folders to hold the mod
    If (!(Test-Path -Path $PDModDir)) { New-Item -ItemType Directory -Path "$PDModDir" }
    New-Item -ItemType Directory -Path "$SourceDir" -Force

    # Copy the silence files
    Copy-Item "$PSScriptRoot\silence" -Destination "$PDModDir" -Recurse -Filter '*.stream'
}


Function Prepare-Files($SourceDir = $SourceDir, $Converter = $Converter)
{
    # Recusively rename all .stream files to .wav
    Get-ChildItem $SourceDir -Recurse -Filter '*.stream' | Rename-Item -NewName { $_.Name.replace(".stream",".wav") }

    # Create "converted" folder inside each folder (only 1 level, not recursive)
    Get-ChildItem $SourceDir | Where-Object { ($_.PSIsContainer) } | % { $_.FullName } | % { New-Item -ItemType Directory -Path "$_\converted" -ErrorAction Ignore }

    # Decode ALL files recursively. Successful decodes are strored in the "converted" folder
    $Files = Get-ChildItem $SourceDir -Recurse -Filter '*.wav'
    $n = 0
    foreach ($File in $Files) {
        $n++
        Write-Progress -Activity 'Decoding Files' -CurrentOperation $File.Directory.Name -PercentComplete (($n / $Files.count) * 100)
        &$Converter "-d" $($File.FullName) $($File.DirectoryName + "\converted\" + $File.Name) | Out-Null
    }

    # Identify which files were successfully converted and remove them from the root directory
    Get-ChildItem $SourceDir -Recurse -Filter 'converted' | Where-Object { ($_.PSIsContainer) } | Get-ChildItem -Filter '*.wav' | ForEach-Object { Remove-Item -Path "$(Split-Path $_.DirectoryName -Parent)\$($_.Name)" }
}


Function Generate-CSV($SourceDir = $SourceDir, $CSVFile = $CSVFile)
{
    Import-Module $PSScriptRoot\lib\mptag\mptag
    $Results = @()
    $Files = Get-ChildItem -Path "$SourceDir\*.wav" -Recurse

    $n = 0
    foreach($File in $Files)
    {
      If ($File.DirectoryName -Match ".\\converted") { $Converted = "TRUE"; $Folder = Split-Path $File.Directory -Parent | Split-Path -leaf } else { $Converted = "FALSE"; $Folder = Split-Path $File.Directory -Leaf }
      $n++
      Write-Progress -Activity 'Gathering Media Info for CSV' -CurrentOperation $Folder -PercentComplete (($n / $Files.count) * 100)

      $MediaInfo = $File | Get-MediaInfo | Select Duration, AudioBitrate, AudioSampleRate, AudioChannels

      $Properties = @{
        Name = $File.name
        Word = ""
        Character = ""
        Text = ""
        Action = ""
        ReplacedBy = ""
        Size = $File.length
        Duration = $MediaInfo.duration
        Bitrate = $MediaInfo.AudioBitrate
        SampleRate = $MediaInfo.AudioSampleRate
        Channels = $MediaInfo.AudioChannels
        Directory = $Folder
        Converted = $Converted
      }
      $Results += New-Object psobject -Property $Properties
    }
    $Results | Select-Object Name,Word,Character,Text,Action,ReplacedBy,Size,Duration,Bitrate,SampleRate,Channels,Directory,Converted | Export-Csv -notypeinformation -Path $CSVFile
}


Function ReEncode-Sounds($Converter = $Converter, $ReEncodePath = $ReEncodePath)
{
    If (!(Test-Path -Path $ReEncodePath)) { New-Item -ItemType Directory -Path "$ReEncodePath" }

    # Can add -Recurse to Get-ChildItem if necessary.
    # If you get an error similar to this: "[ERROR] Input file must be a well formed signed 16bit PCM WAV file" it is probably already a stream file, and will be renamed without problem.
    Get-ChildItem "$ReEncodePath" -Filter '*.wav' | ForEach-Object { Write-Output "Converting $($_.FullName)"; &$Converter "-e" $($_.FullName) $($_.DirectoryName + "\" +  $_.BaseName + ".stream") }
}


# Populate default argument values
$ModName = "FilthyFilter"
$PDModDir = "D:\pd2mod"
$SourceDir = "$PDModDir\source"
$Converter = "$PSScriptRoot\lib\sound_converter\wwise_ima_adpcm.exe"
$CSVFile = "$PDModDir\files.csv"
$ReEncodePath = "$PDModDir\$ModName"